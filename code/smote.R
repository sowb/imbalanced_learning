#######################################################
# file: SMOTE                           ###############
# Author: Ali HARCH, Boubacar SOW, Gauthier CASTRO   ##
# nov.-dec 2018                         ###############
#######################################################

# SMOTE 
# dataset: full dataset
# ycol: column index of the class attribute
# N: % Amount of SMOTE
# k: Number of nearest
# -> return: (N/100) * obs synthetic minority class samples
SMOTE <- function(dataset, ycol, N = 100, k = 4) {
  # min and majority class values
  target <- as.data.frame(table(dataset[,ycol]))
  minority <- target[order(target$Freq), ][1, 1]
  majority <- target[order(target$Freq), ][2, 1]
  
  # indexes of minority class samples
  min_indexes <- which(dataset[,ycol]==minority)
  
  # size of minority classes
  ms <- length(min_indexes)
  
  # handle N<100 which means undersampling minority
  if (N<100) {
    ms <- as.integer((N/100) * ms)
    min_indexes <- min_indexes[sample(nrow(ms)),]
    N <- 100
  }
  
  # distance matrix between observations
  dist_mat <- as.matrix(dist(dataset[min_indexes,-ycol]))
  
  numattrs <- ncol(dataset) - 1
  N <- as.integer(N/100)
  
  # output matrix
  synthetic <- matrix(NA, nrow = ms*N, ncol = numattrs)
  newindex <- 1
  syn <- list(synthetic=synthetic, newindex=newindex)
  
  # produce N samples for earch i
  for (i in 1:ms) {
    # distances for this observation
    neighbors_dist <- dist_mat[i,]
    neighbors <- knnindexes(neighbors_dist, i, k)
    
    # get the k-nearest neighbors
    nnarray <- dataset[min_indexes[neighbors],-ycol]
    # populate the synthetic matrix
    syn <- populate(N, dataset[min_indexes[i],-ycol], nnarray, syn)
  }
  syn$synthetic <- cbind(syn$synthetic, dataset[min_indexes[1],ycol])
  return(syn$synthetic)
}
  
# get k-nn indexes
# d: dist vector
# i: self dist to be removed
# k: number of neighbors
# -> returns indexes of k nn
knnindexes <- function(d, i, k){
  ret <- which(d <= sort(d, partial=k+1)[k+1])
  # remove self from neighbors index list
  ret <- ret[ret!=i]
  return(ret)
}

# populate function which creates N synthetics
# N: number of synthetics to create
# model: current observation from which we populate
# nnarray: list of neighbors to be considered for populate
# syn: list of array to feed and its current index
populate <- function(N, model, nnarray, syn){
  for (ns in 1:N) {
    # pick a neighbor index
    nn <- nnarray[sample(nrow(nnarray), 1),]
    for (attr in 1:length(model)) {
      gap <- runif(1)
      syn$synthetic[syn$newindex, attr] <- as.numeric((1-gap)*model[attr] + gap*nn[attr])
    }
    syn$newindex <- syn$newindex + 1
  }
  return(syn)
}

# adapted from mlbench.circle add ratio
cir <- function(n=100, d=2, r=0.79){
  x <- matrix(runif(n * d, -1, 1), ncol = d, nrow = n)
  if ((d != as.integer(d)) || (d < 2)) 
    stop("d must be an integer >=2")
  z <- rep(1, length = n)
  z[apply(x, 1, function(x) sum(x^2)) > r^2] <- 2
  retval <- list(x = x, classes = factor(z))
  class(retval) <- c("mlbench.circle", "mlbench")
  retval
}

# test SMOTE with cir data
test_SMOTE <- function(N = 200, k = 3, n=1000, d=0.4){
  x <- cir(n, 2, d)
  ds <- cbind(x$x, x$classes)
  df <- data.frame(ds)
  y <- 3
  #undebug(SMOTE)
  synthetic_data = SMOTE(df, ycol = y, N, k)
  #synthetic_data_s <- smotefamily::SMOTE(df[,1:2], df[,3], K = 3, 5)
  
  plot(x=ds[,1], y=ds[,2], col=(ds[,3]-1), pch=18)
  points(synthetic_data[,1], synthetic_data[,2], col='yellow', pch=18)
  #points(synthetic_data_s$syn_data$X1, synthetic_data_s$syn_data$X2, col='green', pch=18)
  points(df$X1[df$X3==1], df$X2[df$X3==1], col='red', type = 'p')
}

#test_SMOTE(300, 3, 1000, 0.4)
