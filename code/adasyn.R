#######################################################
# file: ADASYN                          ###############
# Author: Ali HARCH, Boubacar SOW, Gauthier CASTRO   ##
# nov.-dec 2018                         ###############
#######################################################


library(pdist)

# ADASYN
# dataset: full dataset
# ycol: column index of the class attribute
# k: k nearest neighbors to consider
# beta: target balancement (1 for 50/50)
adasyn <- function(dataset, ycol, k = 3, beta = 1){
  # get min and majority class values
  target <- as.data.frame(table(dataset[,ycol]))
  minority <- target[order(target$Freq), ][1, 1]
  majority <- target[order(target$Freq), ][2, 1]
  
  # indexes of minority class samples
  min_indexes <- which(dataset[,ycol]==minority)
  
  # size of minority classes
  ms <- length(min_indexes)
  ml <- sum(dataset[,ycol]==majority)
  
  # generation factor
  gsyn <- (ml-ms)*beta
  
  # distance & nearest neighboors
  dist_mat <- as.matrix(pdist(dataset[min_indexes,-ycol], dataset[,-ycol]))
  knn_mat <- get_knn_indexes(dist_mat, min_indexes,  k)
  r <- rep(NA, length = ms)
  
  models <- min_indexes
  for (min_obs_i in ms:1) {
    delta <- sum(dataset[knn_mat[min_obs_i,], ycol]==majority)
    if (delta==k | delta==0) {
      # if xi surrounded by majority or minority samples: 
      # considered as noise or irrelevant, drop it from models
      models <- models[-min_obs_i]
      dist_mat <- dist_mat[-min_obs_i,,drop=F]
      knn_mat <- knn_mat[-min_obs_i,,drop=F]
      r <- r[-min_obs_i]
    } else {
      r[min_obs_i] <- delta / k
    }
  }
  if(length(r)==0){
    message("can't populate from the examples.")
    return(-1)
  }
  # Normalise
  r <- r/sum(r)
  
  # Calculate the number of synthetic data examples that
  # need to be generated for each minority example
  g <- round(r * gsyn)
  
  # Populate s synthetic metric of new syn samples
  s <- matrix(NA, nrow = sum(g), ncol = (ncol(dataset)-1))
  newindex <- 0
  for (i in 1:length(models)) {
    obs_i <- models[i]
    if (g[i]!=0) {
      for (gi in 1:g[i]) {
        newindex <- newindex + 1
        nns_min <- intersect(min_indexes, knn_mat[i, ])
        neighb <- nns_min[sample.int(length(nns_min), 1)]
        xzi <- dataset[neighb,-ycol]
        s[newindex,] <- as.matrix(dataset[obs_i,-ycol] + (xzi - dataset[obs_i,-ycol]) * runif(1))
      }      
    }

  }
  res <- cbind(s, minority)
  return(res)
}

# NN indexes
get_knn_indexes <- function(dist_mat, indexes, k){
  dist_mat <-as.data.frame(dist_mat)
  colnames(dist_mat) <- as.character(1:ncol(dist_mat))
  # empty nearest neighbors matrix to be fed
  nns_indexes <- matrix(rep(NA, length = nrow(dist_mat) * (k) ), nrow = nrow(dist_mat))
  # get the knn for each row of the distance matrix
  for (i in 1:nrow(dist_mat)) {
    nns_indexes[i,] <- as.integer(names(sort(dist_mat[i,], partial=1:(k+1))[2:(k + 1)]))
  }
  nns_indexes <- as.matrix(nns_indexes, ncol = k, byrow = T)
  return(nns_indexes)
}
### simulate data

# adapted from mlbench.circle add ratio
cir <- function(n=100, d=2, r=0.79){
  x <- matrix(runif(n * d, -1, 1), ncol = d, nrow = n)
  if ((d != as.integer(d)) || (d < 2)) 
    stop("d must be an integer >=2")
  z <- rep(1, length = n)
  z[apply(x, 1, function(x) sum(x^2)) > r^2] <- 2
  retval <- list(x = x, classes = factor(z))
  class(retval) <- c("mlbench.circle", "mlbench")
  retval
}

# test function for adasyn
test_adasyn <- function(k = 3, beta=1, n=1000, d=0.4){
  x <- cir(n, 2, d)
  
  ds <- cbind(x$x, x$classes)
  df <- data.frame(ds)
  ycol <- 3
  
  #debug(get_knn_indexes)
  #debug(adasyn)
  synthetic_data <- adasyn(ds, ycol, k, 1)
  synthetic_data_s <- smotefamily::ADAS(df[,1:2], df[,3], K = 3)
  
  plot(x=ds[,1], y=ds[,2], col=(ds[,3]-1), pch=18)
  points(synthetic_data[,1], synthetic_data[,2], col='yellow', pch=18)
  points(synthetic_data_s$syn_data$X1, synthetic_data_s$syn_data$X2, col='green', pch=18)
  points(df$X1[df$X3==1], df$X2[df$X3==1], col='red', type='p')
}
#test_adasyn()
